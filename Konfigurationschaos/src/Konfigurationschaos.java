
public class Konfigurationschaos{
	
	public static void main(String[] args){

		int cent;
		int euro;
		int summe;
		
		int muenzenCent = 1280;
		int muenzenEuro = 130;
			summe = muenzenCent + muenzenEuro * 100;
		
			cent = summe % 100;
			euro = summe / 100;
		
		String typ = "Automat AVR";
		String bezeichnung = "Q2021_FAB_A";
		String name;
			name = typ + " " + bezeichnung;
		
		char sprachModul = 'd';
		byte PRUEFNR = 4; 
		
		double patrone = 46.24;
		double maximum = 100.00;
		double fuellstand = maximum - patrone;
		
		final boolean statusCheck;
			statusCheck = (euro <= 150) 
		
				&& (fuellstand >= 50.00) 
				&& (euro >= 50)
				&& (cent != 0)
				&& (sprachModul == 'd')
				&&  (PRUEFNR != 5 || PRUEFNR != 6);
			
			System.out.println("Name: " + name);
			System.out.println("Sprache: " + sprachModul);
			System.out.println("Pr�fnummer : " + PRUEFNR);
			System.out.println("F�llstand Patrone: " + fuellstand + " %");
			System.out.println("Summe Euro: " + euro +  " Euro");
			System.out.println("Summe Rest: " + cent +  " Cent");		
			System.out.println("Status: " + statusCheck);
	}

}
