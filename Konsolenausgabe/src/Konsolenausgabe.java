
public class Konsolenausgabe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int nord = 20;
		//strg + f for searching and replacing names
		System.out.print("Ein kr�ftiges \"Moin moin\" aus dem " + nord + " j�hrigen Norden.\n\n");
		System.out.print("Der Norden gr��t mit nem kr�ftigen Moin.\n\n");
		// mit Println wird automatisch ein Zeilenumbruch gesetzt und bei Print nicht.
		
		
		System.out.println("      *\n"
						 + "     ***\n"
						 + "    *****\n"
						 + "   *******\n"
						 + "  *********\n"
						 + " ***********\n"
						 + "*************\n"
						 + "     *** \n"
						 + "     ***  \n\n ");
		
		System.out.printf("%2.2f\n", 22.4234234 );
		System.out.printf("%2.2f%n", 111.2222 );
		System.out.printf("%2.2f%n", 4.0 );
		System.out.printf("%2.2f%n", 1000000.551 );
		System.out.printf("%2.2f%n\n\n", 97.34 );
		
		
		
		System.out.println("   **\n*      *\n*      *\n   **\n\n");
		
		System.out.println("  Fahrenheit  |  Celsius  \n -------------------------\n  -20         |    -28,89  \n  -10         |    -23,33\n    0         |    -17,78\n  +20         |     -6,67\n  +30         |     -1,11");
	}
 
}
