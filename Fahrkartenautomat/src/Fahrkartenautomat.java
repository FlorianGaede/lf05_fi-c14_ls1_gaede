﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
      
       
       System.out.print("Zu zahlender Betrag (EURO): ");
       
       
       zuZahlenderBetrag = fahrkartenbestellungErfassen();
       
       
    		   
    		   // Geldeinwurf
       // -----------
      
      
       
       
       eingezahlterGesamtbetrag = fahrkartenBezahlen(0.0 , zuZahlenderBetrag);
       			// aufruf zu methode (fahrkartenBezahlen)
       
      
       
       
       // Fahrscheinausgabe
       // -----------------
       FahrkartenAusgeben();
       
       
       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
      
      rückgabebetrag(eingezahlterGesamtbetrag, zuZahlenderBetrag);
       
      
      System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
              "vor Fahrtantritt entwerten zu lassen!\n"+
              "Wir wünschen Ihnen eine gute Fahrt.");
      
    }
    
    static double fahrkartenbestellungErfassen ()
    {  
    	Scanner tastatur1 = new Scanner(System.in);
    	double zuZahlenderBetrag = tastatur1.nextDouble(); 

	       System.out.println("Anzahl der Fahrkarten:");
	       
	       int Anzahl = tastatur1.nextInt();
	       zuZahlenderBetrag = zuZahlenderBetrag * Anzahl;
	       
     return (zuZahlenderBetrag); 
     }
    
    static double fahrkartenBezahlen(double eingezahlterGesamtbetrag, double zuZahlenderBetrag)
    {  
    	
		 
    	 while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
         {
    		 double eingeworfeneMünze;
    		 
    		 Scanner tastatur2 = new Scanner(System.in);
    		 
    		 System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
      	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
      	   eingeworfeneMünze = tastatur2.nextDouble();
             eingezahlterGesamtbetrag += eingeworfeneMünze;
        
         }
             return (eingezahlterGesamtbetrag);
          
    }

    
    static void FahrkartenAusgeben()
    {  
    	System.out.println("\nFahrschein/e wird ausgegeben");
    
    for (int i = 0; i < 8; i++)
    {
       System.out.print("=");
       try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    System.out.println("\n\n");
	}

    static void rückgabebetrag(double eingezahlterGesamtbetrag, double zuZahlenderBetrag)
    		{ 	
	double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	
	if(rückgabebetrag > 0.0)
		{		
	String rückgabebetr = String.format("%.2f", rückgabebetrag);
	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetr + " EURO");
	   System.out.println("wird in folgenden Münzen ausgezahlt:");

    while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
    {
 	  System.out.println("2 EURO");
       rückgabebetrag -= 2.0;
    }
    while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
    {
 	  System.out.println("1 EURO");
       rückgabebetrag -= 1.0;
    }
    while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
    {
 	  System.out.println("50 CENT");
       rückgabebetrag -= 0.5;
    }
    while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
    {
 	  System.out.println("20 CENT");
        rückgabebetrag -= 0.2;
    }
    while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
    {
 	  System.out.println("10 CENT");
       rückgabebetrag -= 0.1;
    }
    while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
    {
 	  System.out.println("5 CENT");
        rückgabebetrag -= 0.05;
    }
    
			}



}
}
